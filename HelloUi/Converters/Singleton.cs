﻿using System;

namespace HelloUi.Converters
{
    /// 单例模式
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Singleton<T> where T : new()
    {
        private static T m_instance;
        private static readonly object sync = new object();
        private Singleton() { }

        /// <summary>
        /// 用法：Singleton<myclass>.GetInstance 获取该类的单例 
        /// </summary>
        public static T GetInstance
        {
            get
            {
                if (m_instance == null)
                {
                    lock (sync)
                    {
                        if (m_instance == null)
                        {
                            m_instance = new T();
                        }
                    }
                }
                return m_instance;
            }
        }
    }
}
